<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @Modified by Amir Ali Moaddeli
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");



/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
         } else {
           $this->db = new PDO('mysql:host=localhost;
							dbname=test;charset=utf8', 
												'amir', 'banan12',
                                                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
           }
        }
    
	

   /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
      public function getBookList()
    {
		$book = array();
		
		$stmt = $this->db->query("SELECT * FROM book");
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$book[]= new Book($row['title'], $row['author'], $row['description'], $row['id']);    
        }
        return $book;	
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        self::verifyId($id);
        $book = null;
        $stmt = $this->db->query("SELECT * FROM book WHERE id = +$id");

        $tuple = $stmt->fetch(PDO::FETCH_ASSOC);
            
        if($tuple){
            $book= new Book($tuple['title'], $tuple['author'], $tuple['description'], $tuple['id']); 
            }   
                 return $book;      
    }
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
        self::verifyBook($book);
        
        $query = 'INSERT INTO book(title, author, description) VALUES(:title,:author,:description)';
    
        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':title',$book->title);
        $stmt->bindValue(':author',$book->author);
        $stmt->bindValue(':description',$book->description);
    
        $stmt->execute();

        $book->id = $this->db->lastInsertId();
    
    }


    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
        self::verifyBook($book);
        $query = ("UPDATE book SET title = :title, author = :author, description = :description WHERE id = :id");
       
        $stmt = $this->db->prepare($query);
       
        $stmt->bindValue(':title',$book->title);
        $stmt->bindValue(':author',$book->author);
        $stmt->bindValue(':description',$book->description);
        $stmt->bindValue(':id',$book->id);
        
        $stmt->execute();
       
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
            self::verifyId($id);
        
            $query = 'DELETE FROM book WHERE id = :id';
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
        
    }
}