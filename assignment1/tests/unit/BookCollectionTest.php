<?php

require_once('D:\Xampp\htdocs\IMT2571\assignment1\Model\DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'amir',
                'banan12',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals($bookList[0]->id, 1);
        $this->assertEquals($bookList[0]->title, 'Jungle Book');
        $this->assertEquals($bookList[1]->id, 2);
        $this->assertEquals($bookList[1]->author, 'J. Walker');
        $this->assertEquals($bookList[2]->id, 3);
        $this->assertEquals($bookList[2]->description, 'Written by some smart gal.');
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book.');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals(4, $book->id);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    
    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
        $testValues = ['title'=>'NewBook','author'=>'Database','description'=>'','id'=>'NotNumeric'];
        $book = new Book($testValues['title'],$testValues['author'],$testValues['description'],$testValues['id']);
        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    }

    // Tests that adding a book fails if mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
        $testValues = ['title'=>'','author'=>'Database','description'=>'','id'=>''];
        $book = new Book($testValues['title'],$testValues['author'],$testValues['description'],$testValues['id']);
        try{
        $this->dbModel->addBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};

    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
       
        $id = 3;
        $testValues = [['title' => "New book", 'author' => "Some author",'description' => "Some description"], 
                       [ 'title'=>"New book", 'author'=>"Some author", 'description'=>""], 
                       ['title'=>"<script>document.body.style.visibility='hidden'</script>",
                          'author'=>"<script>document.body.style.visibility='hidden'</script>",
                          'description'=>"<script>document.body.style.visibility='hidden'</script>"]];

        foreach ($testValues as $tuple) {
            $book = new Book($tuple['title'], $tuple['author'], $tuple['description']);
            $this->dbModel->addBook($book);
        }
        // checks if id was successfully added
        $this->tester->seeNumRecords(6, 'book');

        foreach ($testValues as $tuple) {
            $id++;
      
        // looks in DB to see if they were added
        $this->tester->seeInDatabase('book', ['id'=>$id,
                                              'title' => $tuple['title'],
                                              'author' => $tuple['author'],
                                              'description' => $tuple['description']]);
        }     
           
    } 
    
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
        $testValues = ['title' => "NewBook2", 'author' => "Database2", 'description' => "", 'id' => "invalid" ];
        $book = new book($testValues['title'],$testValues['author'], $testValues['description'], $testValues['id']);
        try{ 
        $this->dbModel->modifyBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    }
 
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {  
        $testValues = [['title' => "New Book", 'author' => "", 'description' => "", 'id' => 1],
                        ['title'=>"",'author' => " author", 'description' => "", 'id' => 2]];
        foreach ($testValues as $tuples) {
            $book = new book($tuples['title'],$tuples['author'], $tuples['description'], $tuples['id']);
            
            try{
            $this->dbModel->modifyBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class,null);
            }catch(InvalidArgumentException $e){};  
          }
    }
    
  
    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
        $id = 3;
        $this->dbModel->deleteBook($id);
        //checks if it's not there
        $this->tester->dontSeeInDatabase('book',['id'=> $id]);
    }
    
    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
        $id = "invalidID";
        try {
        $this->dbModel->deleteBook($id);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    } 
}